## Patient-Centered Care System (Diabates Type 2)

This README provides a step-by-step guide for implementing a patient-centered care system using a Patient Knowledge Graph (PKG) integrated with clinical data, decision support, and scheduling conflict detection.
System Overview

The patient-centered care system leverages a Patient Knowledge Graph (PKG) to store and manage patient-related data, including medical history, appointments, preferences, and personal commitments. Integration with clinical data standards like FHIR/RDF and semantic web rule language (SWRL) enables intelligent decision support and proactive patient management.

### Step-by-Step Implementation
```r
pkg:patientMaria rdf:type fhir:Patient ;
    fhir:identifier [ 
        fhir:use fhir:official ;
        fhir:system "http://example.com/patientid" ;
        fhir:value "123456"
    ] .

pkg:practitionerBianchi rdf:type fhir:Practitioner ;
    fhir:identifier [ 
        fhir:use fhir:official ;
        fhir:system "http://example.com/practitionerid" ;
        fhir:value "789012"
    ] .
```
#### Initial Consultation and Medical Data Entry

- Date: 2024-05-01 - 09:00
- Event: Patient's initial visit to the healthcare provider.
- Role:
    - Practitioner: Dr. Luigi Bianchi
- Action:
    - Practitioner: Records patient demographics, medical conditions, and prescribed medications in the FHIR/RDF knowledge graph.
- Data Entry:
    - Patient's diagnoses of Type 2 Diabetes, mild depression, and gastritis are stored in the knowledge graph.

    ```r
    pkg:consultation1 rdf:type fhir:Encounter ;
    fhir:status fhir:finished ;
    fhir:subject pkg:patientMaria ;
    fhir:participant pkg:practitionerBianchi ;
    fhir:period [ 
        fhir:start "2024-05-01T09:00:00"^^xsd:dateTime ;
        fhir:end "2024-05-01T09:30:00"^^xsd:dateTime
    ] .
    ```
    ```r
    pkg:diagnosis1 rdf:type fhir:Condition ;
    fhir:subject pkg:patientMaria ;
    fhir:code [ 
        fhir:coding [ 
            fhir:system "http://snomed.info/sct" ;
            fhir:code "44054006" ;
            fhir:display "Type 2 Diabetes Mellitus"
        ]
    ] .

    pkg:diagnosis2 rdf:type fhir:Condition ;
        fhir:subject pkg:patientMaria ;
        fhir:code [ 
            fhir:coding [ 
                fhir:system "http://snomed.info/sct" ;
                fhir:code "35489007" ;
                fhir:display "Mild depression"
            ]
        ] .

    pkg:diagnosis3 rdf:type fhir:Condition ;
        fhir:subject pkg:patientMaria ;
        fhir:code [ 
            fhir:coding [ 
                fhir:system "http://snomed.info/sct" ;
                fhir:code "67406007" ;
                fhir:display "Gastritis"
            ]
        ] .
    ```

#### Medication Prescription and Decision Support
- Date: 2024-05-01 - 09:30
- Event: Prescribing medications for the patient's
- Role:
    - Practitioner: Dr. Luigi Bianchi
    - Reasoner/Assistant: System
 conditions.
- Action:
    - Practitioner: Prescribes medications and specifies dosage and administration route.
    - Reasoner/Assistant: Infers appropriate injection route considering patient's gastritis condition.
- Inference: 
    - ~~The reasoner detects Maria's conditions and infers recommendations for lifestyle changes, such as increased physical activity to manage diabetes and depression.~~
- Data Entry:
    - Medication requests are added to the knowledge graph, including dosage and administration route.

    ```r
    pkg:prescription1 rdf:type fhir:MedicationRequest ;
    fhir:subject pkg:patientMaria ;
    fhir:authoredOn "2024-05-01T09:30:00"^^xsd:dateTime ;
    fhir:medicationCodeableConcept [ 
        fhir:coding [ 
            fhir:system "http://www.nlm.nih.gov/research/umls/rxnorm" ;
            fhir:code "320325" ;
            fhir:display "Metformin 500mg tablet"
        ]
    ] .

    pkg:prescription2 rdf:type fhir:MedicationRequest ;
        fhir:subject pkg:patientMaria ;
        fhir:authoredOn "2024-05-01T09:30:00"^^xsd:dateTime ;
        fhir:medicationCodeableConcept [ 
            fhir:coding [ 
                fhir:system "http://www.nlm.nih.gov/research/umls/rxnorm" ;
                fhir:code "197665" ;
                fhir:display "Sertraline 50mg tablet"
            ]
        ] .

    pkg:prescription3 rdf:type fhir:MedicationRequest ;
        fhir:subject pkg:patientMaria ;
        fhir:authoredOn "2024-05-01T09:30:00"^^xsd:dateTime ;
        fhir:medicationCodeableConcept [ 
            fhir:coding [ 
                fhir:system "http://www.nlm.nih.gov/research/umls/rxnorm" ;
                fhir:code "779635" ;
                fhir:display "Lansoprazole 15mg capsule"
            ]
        ] .
    ```

#### Home Monitoring and Inference Generation

- Date: 2024-05-15 - Ongoing
- Role:
    - Patient: Maria Rossi
    - Reasoner/Assistant: System
-    Event: Patient monitors health parameters remotely.
- Action:
    - Patient: Records blood glucose levels and other vital signs using a mobile app.
    - Reasoner/Assistant: Generates inferences for treatment adjustments or reminders based on updated patient data.
- Inference:
    - The reasoner analyzes Maria's medication adherence and detects patterns of non-adherence, generating reminders for medication adherence.
    - Additionally, the reasoner infers the impact of social isolation on Maria's mental health and suggests interventions or support resources.
- Data Entry:
    - Patient's glucose levels and vital signs are updated in the knowledge graph, triggering inference generation by the reasoner.
    ```r
    pkg:monitoringEvent1 rdf:type fhir:Observation ;
    fhir:subject pkg:patientMaria ;
    fhir:code [ 
        fhir:coding [ 
            fhir:system "http://loinc.org" ;
            fhir:code "2339-0" ;
            fhir:display "Blood glucose"
        ]
    ] ;
    fhir:valueQuantity [ 
        fhir:value "120" ;
        fhir:unit "mg/dL" ;
        fhir:system "http://unitsofmeasure.org" ;
        fhir:code "mg/dL"
    ] ;
    fhir:effectiveDateTime "2024-05-15T09:00:00"^^xsd:dateTime .
    ```

#### Appointment Scheduling and Conflict Detection

- Date: Ongoing - As needed
- Event: Scheduling patient appointments for follow-up visits.
- Role:
    - Patient: Maria Rossi
    - Practitioner's Assistant: Dr. Bianchi's assistant
    - Reasoner/Assistant: System
- Action:
    - Patient: Schedules personal appointments.
    - Practitioner's Assistant: Schedules patient appointments and integrates them with the PKG.
    - Reasoner/Assistant: Detects scheduling conflicts and alerts the practitioner's assistant for resolution.
- Data Entry:
    - Appointments are recorded in the scheduling system and integrated with the PKG.
    ```r
    pkg:appointment1 rdf:type fhir:Appointment ;
    fhir:participant pkg:patientMaria, pkg:practitionerAssistant, pkg:reasoner3 ;
    fhir:start "2024-05-30T10:00:00"^^xsd:dateTime ;
    fhir:end "2024-05-30T10:30:00"^^xsd:dateTime .

    pkg:practitionerAssistant rdf:type fhir:Practitioner ;
        fhir:identifier [ 
            fhir:use fhir:official ;
            fhir:system "http://example.com/practitionerassistantid" ;
            fhir:value "1234567"
        ] .
    ```